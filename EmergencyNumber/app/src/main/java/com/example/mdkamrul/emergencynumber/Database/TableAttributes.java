package com.example.mdkamrul.emergencynumber.Database;

/**
 * Created by mdkamrul on 18-Jan-17.
 */

public class TableAttributes {
    public static final String TABLENAME="addInfo";
    public static final String ORGANIZATION="organizationName";
    public static final String PHONE="phone";
    public String createAddInfoTableQuery() {
        return "Create Table "+TABLENAME+"(info_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                ORGANIZATION+" TEXT," +
                PHONE+" TEXT) ";

    }
}
