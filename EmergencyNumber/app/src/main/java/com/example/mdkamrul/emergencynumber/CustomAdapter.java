package com.example.mdkamrul.emergencynumber;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.mdkamrul.emergencynumber.Model.EmergencyInformation;

import java.util.ArrayList;

/**
 * Created by mdkamrul on 18-Jan-17.
 */

public class CustomAdapter extends ArrayAdapter<EmergencyInformation> {
    ArrayList<EmergencyInformation> informationArrayList;

    public CustomAdapter(Context context,ArrayList<EmergencyInformation> informationArrayList) {
        super(context,R.layout.customadapterlayout, informationArrayList);
        this.informationArrayList=informationArrayList;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {




        EmergencyInformation taskEntry= new EmergencyInformation();
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View customView = inflater.inflate(R.layout.customadapterlayout,parent,false);
        TextView textViewOrganizationName = (TextView)customView.findViewById(R.id.textViewOrganizationName);
        final TextView textViewCallNum = (TextView)customView.findViewById(R.id.textViewPhoneNum);
        Button buttonCall = (Button) customView.findViewById(R.id.buttonCall);
        textViewOrganizationName.setText(informationArrayList.get(position).getOrganizationName());
        textViewCallNum.setText(informationArrayList.get(position).getPhoneNo());
        buttonCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phone = textViewCallNum.getText().toString();
                Intent intent = new Intent(Intent.ACTION_DIAL,Uri.fromParts("tel",phone,null));
                parent.getContext().startActivity(intent);
            }
        });

        return  customView;

    }


}
