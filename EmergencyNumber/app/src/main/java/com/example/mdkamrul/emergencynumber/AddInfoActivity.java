package com.example.mdkamrul.emergencynumber;

import android.app.ActionBar;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;

import com.example.mdkamrul.emergencynumber.Database.DatabaseHelper;
import com.example.mdkamrul.emergencynumber.Model.EmergencyInformation;

public class AddInfoActivity extends AppCompatActivity {
        EditText editTextOrganaizationName;
        EditText editTextPhoneNo;
        DatabaseHelper db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_info);

        db = new DatabaseHelper(AddInfoActivity.this);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        editTextOrganaizationName = (EditText)findViewById(R.id.editTextOrganaizationName);
        editTextPhoneNo=(EditText)findViewById(R.id.editTextPhoneNumber);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.addinfo,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.itemDone:
                String name = editTextOrganaizationName.getText().toString();
                String phone = editTextPhoneNo.getText().toString();
                EmergencyInformation information = new EmergencyInformation();
                information.setOrganizationName(name);
                information.setPhoneNo(phone);
                db.insertInfoQuery(information);
                Intent intent = new Intent(AddInfoActivity.this, MainActivity.class);
                startActivity(intent);
            case android.R.id.home:
                Intent intentMainActivity = new Intent(AddInfoActivity.this, MainActivity.class);
                startActivity(intentMainActivity);
                return true;

            default:
                return super.onOptionsItemSelected(item);


        }
    }
}
