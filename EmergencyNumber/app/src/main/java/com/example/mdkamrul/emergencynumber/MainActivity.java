package com.example.mdkamrul.emergencynumber;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.LayoutInflaterCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.mdkamrul.emergencynumber.Database.DatabaseHelper;
import com.example.mdkamrul.emergencynumber.Model.EmergencyInformation;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    ListView listViewInfoList;
    FloatingActionButton fab;
    DatabaseHelper db;
    ArrayList<EmergencyInformation> infoList;
    ArrayAdapter<EmergencyInformation> infoArrayAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listViewInfoList = (ListView)findViewById(R.id.listViewInfo);
        fab  = (FloatingActionButton)findViewById(R.id.fabAddInfo);
        db= new DatabaseHelper(MainActivity.this);
        infoList=db.getAllInfo ();
        if(infoList==null){
            Toast.makeText(MainActivity.this, "No Number Added", Toast.LENGTH_LONG).show();
        }
        if (infoList !=null) {
            infoArrayAdapter = new CustomAdapter(MainActivity.this, infoList);
            listViewInfoList.setAdapter(infoArrayAdapter);
        }

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,AddInfoActivity.class);
                startActivity(intent);
            }
        });

    }
}
