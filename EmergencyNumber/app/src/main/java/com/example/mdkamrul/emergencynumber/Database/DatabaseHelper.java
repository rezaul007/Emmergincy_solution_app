package com.example.mdkamrul.emergencynumber.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.mdkamrul.emergencynumber.Model.EmergencyInformation;

import java.util.ArrayList;

import static android.R.attr.theme;
import static android.R.attr.track;
import static android.R.attr.version;

/**
 * Created by mdkamrul on 18-Jan-17.
 */

public class DatabaseHelper extends SQLiteOpenHelper{
    public static final String DATABASENAME ="addInfo.db";

    public DatabaseHelper(Context context) {
        super(context, DATABASENAME, null, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        TableAttributes tableAttributes = new TableAttributes();
        String addInfoQuery = tableAttributes.createAddInfoTableQuery ();
        try {
            db.execSQL(addInfoQuery);
            Log.i("Create Table: ","Done");
        }catch (SQLiteException e){
            Log.i("Error Table Creation",e.toString());
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void insertInfoQuery(EmergencyInformation information) {
        SQLiteDatabase insertInfoQuery = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TableAttributes.ORGANIZATION,information.getOrganizationName());
        values.put(TableAttributes.PHONE,information.getPhoneNo());
        try {
            insertInfoQuery.insert(TableAttributes.TABLENAME,null,values);
            Log.i("Value Inserted","Values are done");
        }catch (SQLiteException e){
            Log.i("Error Insertion",e.toString());
        }
    }

    public ArrayList<EmergencyInformation> getAllInfo() {
        ArrayList<EmergencyInformation> infoList= new ArrayList<EmergencyInformation>();
        SQLiteDatabase fetchAllInfo = this.getReadableDatabase();
        String allInfo = "Select * From " +TableAttributes.TABLENAME;
        Cursor cursor = fetchAllInfo.rawQuery(allInfo,null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            EmergencyInformation info= new EmergencyInformation();
            info.setOrganizationName(cursor.getString(cursor.getColumnIndex(TableAttributes.ORGANIZATION)));
            info.setPhoneNo(cursor.getString(cursor.getColumnIndex(TableAttributes.PHONE)));
            infoList.add(info);
            cursor.moveToNext();

        }

        return infoList;
    }
}
