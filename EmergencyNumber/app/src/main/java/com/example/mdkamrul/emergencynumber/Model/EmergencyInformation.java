package com.example.mdkamrul.emergencynumber.Model;

/**
 * Created by mdkamrul on 18-Jan-17.
 */

public class EmergencyInformation {
    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    @Override
    public String toString() {
        return "EmergencyInformation{" +
                "organizationName='" + organizationName + '\'' +
                ", phoneNo='" + phoneNo + '\'' +
                '}';
    }

    private String organizationName;
    private String phoneNo;

}
